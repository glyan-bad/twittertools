# This file contains miscellaneous functions and tools to mine tweets using tweepy over the Twitter API.

from functools import reduce

import pandas as pd
import plotly.express as px
import requests
import streamlit as st
import streamlit.components.v1 as components
import tweepy
from tabulate import tabulate
import os

from typing import List

from utils import Utils


def explore(method, *args, **kwargs):
    responses = []
    cpt = 0
    try:
        for response in tweepy.Paginator(method, *args, **kwargs):
            responses += response
            cpt += 1
            if kwargs['limit'] and cpt >= int(kwargs['limit']):
                return responses

    except:
        return responses
    return responses


def getMedia(tweets, single_tweet=False):
    try:
        if not single_tweet:
            tweetMedia = {m['id']: list(m.data['attachments']['media_keys']) for m in
                          filter(lambda t: t if 'attachments' in t else {}, tweets.data)}
            media = {m["media_key"]: {'type': m.type, 'url': m.url, 'prev_url': m.preview_image_url} for m in
                     tweets.includes['media']}
            tab = []
            for items in tweetMedia:
                if tweetMedia[items]:
                    for medium in tweetMedia[items]:
                        tab += [
                            ([items, medium, media[medium]['type'], media[medium]['prev_url'], media[medium]['url']])]

            df = pd.DataFrame.from_records(tab, columns=['id', 'media_id', 'type', 'preview_image_url', 'url'])
            return df
        else:
            tweetMedia = {m['id']: list(m.data['attachments']['media_keys']) for m in
                          filter(lambda t: t if 'attachments' in t else {}, tweets)}
        media = {m["media_key"]: {'type': m.type, 'url': m.url, 'prev_url': m.preview_image_url} for m in
                 tweets.includes['media']}
        tab = []
        for items in tweetMedia:
            if tweetMedia[items]:
                for medium in tweetMedia[items]:
                    tab += [([items, medium, media[medium]['type'], media[medium]['prev_url'], media[medium]['url']])]

        df = pd.DataFrame.from_records(tab, columns=['id', 'media_id', 'type', 'preview_image_url', 'url'])
        return df
    except KeyError:
        return pd.DataFrame.from_records([],
                                         columns=['id', 'media_id', 'type', 'preview_image_url', 'url'])


def getTweet(tweet_id, list_mode=False):
    data = Utils.client.get_tweet(tweet_id, tweet_fields=['context_annotations', 'created_at'],
                                  user_fields=Utils.user_fields,
                                  media_fields=Utils.media_fields,
                                  expansions=['attachments.media_keys', 'author_id'])
    if list_mode:
        try:
            return [data.data.id, data.data.text, data.data.author_id]
        except AttributeError:
            return [None, None, None]
    else:
        return data


def tweetsListToDF(tweets):
    df = pd.DataFrame.from_records(tweets,
                                   columns=["id", "text", "author_id", 'created_at'])
    df['tweet_url'] = "https://twitter.com/twitter/statuses/" + df['id'].apply(str)
    df['tweet_type'] = "ORIG"
    return df


def mediaListToDF(media):
    df = pd.DataFrame.from_records(media,
                                   columns=["media_key", "type", "media_url"])
    return df


def tweetsDictToDF(tweets, single_tweet=False):
    if not single_tweet:
        df = pd.DataFrame.from_records(list(
            map(lambda x: [x.id, x.text, x.author_id, x.created_at, "https://twitter.com/twitter/statuses" + x.id],
                tweets.data)),
            columns=["id", "text", "author_id", 'created_at', 'tweet_url'])
        df['tweet_type'] = "ORIG"
        return df
    else:
        df = pd.DataFrame.from_records(
            [[tweets.data.id, tweets.data.text, tweets.data.author_id, tweets.data.created_at,
              "https://twitter.com/twitter/statuses" + tweets.data.id]],
            columns=["id", "text", "author_id", 'created_at', 'tweet_url'])
        df['tweet_type'] = "ORIG"
        return df


def getLikers(tweet_id, limit=1):
    data = explore(Utils.client.get_liking_users, tweet_id, limit=limit, max_results=100, user_fields=Utils.user_fields)
    if data[0]:
        df = pd.DataFrame.from_records(list(
            map(lambda x: [x.created_at, x.description, x.entities, x.id, x.location, x.name, x.pinned_tweet_id,
                           x.profile_image_url, x.protected, x.public_metrics, x.url, x.username, x.verified,
                           x.withheld],
                extractData(data)[1])),
            columns=["created_at", "description", "entities", "id", "location", "name", "pinned_tweet_id",
                     "profile_image_url", "protected", "public_metrics", "url", "username", "verified",
                     "withheld"])
        return df
    else:
        return pd.DataFrame.from_records(
            [[None, None, None, -1, None, None, None, None, None, None, None, None, None, None]],
            columns=["created_at", "description", "entities", "id", "location", "name", "pinned_tweet_id",
                     "profile_image_url", "protected", "public_metrics", "url", "username", "verified",
                     "withheld"])


def mergeTweetsMedia(tweets, media):
    df = tweets.join(media.set_index('id'), on='id')
    return df


def getRetweeters(tweet_id, limit=1):
    data = explore(Utils.client.get_retweeters, tweet_id, limit=limit, max_results=100, user_fields=Utils.user_fields)
    if data[0]:
        df = pd.DataFrame.from_records(list(
            map(lambda x: [x.created_at, x.description, x.entities, x.id, x.location, x.name, x.pinned_tweet_id,
                           x.profile_image_url, x.protected, x.public_metrics, x.url, x.username, x.verified,
                           x.withheld],
                extractData(data)[1])),
            columns=["created_at", "description", "entities", "id", "location", "name", "pinned_tweet_id",
                     "profile_image_url", "protected", "public_metrics", "url", "username", "verified",
                     "withheld"])
        return df
    else:
        return pd.DataFrame.from_records(
            [[None, None, None, -1, None, None, None, None, None, None, None, None, None, None]],
            columns=["created_at", "description", "entities", "id", "location", "name", "pinned_tweet_id",
                     "profile_image_url", "protected", "public_metrics", "url", "username", "verified",
                     "withheld"])


def getQuoters(tweet_id):
    data = explore(Utils.client.get_quote_tweets, tweet_id, limit=1, max_results=100,
                   user_fields=Utils.user_fields,
                   media_fields=Utils.media_fields, expansions=['attachments.media_keys', 'author_id'])
    if data[0]:
        tweets_ids = list(map(lambda x: [x.id, x.author_id], extractData(data)[1]))
        tweets = list(map(lambda x: getTweet(x[0]), tweets_ids))
        tweetsDict = list(map(lambda x: tweetsDictToDF(x, single_tweet=True), tweets))
        tweetsDF = pd.concat(tweetsDict)

        tweetsMedia = list(map(lambda x: getMedia(x, single_tweet=True), tweets))
        mediaDF = pd.concat(tweetsMedia)

        return mergeTweetsMedia(tweetsDF, mediaDF)
    else:
        return mergeTweetsMedia(pd.DataFrame.from_records([[None, None, None]],
                                                          columns=["id", "text", "author_id"]),
                                pd.DataFrame.from_records([[None, None, None, None, None]],
                                                          columns=['id', 'media_id', 'type', 'preview_image_url',
                                                                   'url']))


def getTrends():
    trend_headers = {
        'Authorization': 'Bearer {}'.format(Utils.MY_BEARER_TOKEN)
    }

    trend_params = {
        'id': 1,
    }

    trend_url = 'https://api.twitter.com/1.1/trends/place.json'
    trend_resp = requests.get(trend_url, headers=trend_headers, params=trend_params)
    tweet_data = trend_resp.json()
    # print(tweet_data)
    # forEach(print,tweet_data[0]['trends'])
    nameCount = map(lambda x: [x['name'], x['tweet_volume']], tweet_data[0]['trends'])
    # forEach(print, nameCount)
    return nameCount


def exploreRetweetsLikesQuotes(people, type, followers):
    if len(people) > 0 and people[0] is not None:
        people_marks = []
        fraudulous_weight = len(Utils.checker) / len(Utils.fraudulous)
        for guy in people:
            print("Processing", guy, type)
            # following = explore(Utils.client.get_users_following, guy, max_results=1000, limit=15)

            res = checkBond(guy, Utils.fraudulous, followers)
            res_int = list(map(lambda v: 1 if v else 0, res.values()))
            user_fraudulous = reduce(lambda x, y: x + y, res_int, 0) * fraudulous_weight

            res = checkBond(guy, Utils.checker, followers)
            res_int = list(map(lambda v: 1 if v else 0, res.values()))
            user_checker = reduce(lambda x, y: x + y, res_int, 0)
            people_marks += [[guy, type, user_checker - user_fraudulous]]

        return pd.DataFrame.from_records(people_marks, columns=['id', 'type', 'mark'])
        # return pd.DataFrame.from_records([[None, None, None]], columns=['id', 'type', 'mark'])


def checkBond(id, accounts, followers):
    res = {}
    for acc in accounts:
        res.update({acc: reduce(lambda x, y: x or y,
                                map(lambda x: x == id, followers[followers['account'] == acc]['user_id']))})
    return res


def tweetSocialMark(tweet_id, followers_path):
    # res = theTweet(str(tweet_id))
    # st.caption("tweet address : https://twitter.com/twitter/statuses/" + str(tweet_id))
    # components.html(res, height=700)

    data = pd.concat([analyzeTweet(tweet_id, 'RT', followers_path=followers_path),
                      analyzeTweet(tweet_id, '<3', followers_path=followers_path)])
    return data
    # for kind in ['RT', '<3']:  # , 'QT']:
    #     data = analyzeTweet(tweet_id, kind, followers_path=followers_path)
    #     st.title(kind)
    #     fig = px.histogram(data, x="mark", title=kind)
    #     fig.show()
    #     st.plotly_chart(fig, use_container_width=True)
    #     print(tabulate(data, headers='keys', tablefmt='psql'))
    #     st.dataframe(data.drop('id', axis=1))
    #     data.to_csv(os.getcwd() + '/'+str(tweet_id)+'_'+kind+'.csv')


def countLinks(dataset, accounts, col_name='links'):
    res = dataset.set_index('id').drop('username', axis=1).join(accounts.set_index('id'), on='id')
    count = res.groupby('id')['account'].nunique()
    # The join must be left outer
    out = dataset.join(count, on='id', how='outer')
    out.rename(columns={'account': col_name}, inplace=True)

    return out[['id', col_name]]


def analyzeTweet(tweet_id, kind='RT', followers_path=''):
    retweeters = getRetweeters(tweet_id, limit=2)
    likers = getLikers(tweet_id, limit=2)
    fraudulous_weight = len(Utils.checker) / len(Utils.fraudulous)

    #   quoters = getQuoters(tweet_id)
    #    quoters.rename(columns={'id': 'tweet_id', 'author_id': 'id', 'type': 'media_type'}, inplace=True)
    followers = pd.read_parquet(followers_path)
    followers.rename(columns={'user_id': 'id'}, inplace=True)

    fraud = followers[followers['account'].isin(list(Utils.fraudulous))].drop_duplicates()
    fraud.set_index('id')

    check = followers[followers['account'].isin(list(Utils.checker))].drop_duplicates()
    check.set_index('id')

    if kind == 'RT':
        outCH = countLinks(retweeters, check, col_name='checks')
        outFR = countLinks(retweeters, fraud, col_name='frauds')
        links = outFR.join(outCH.set_index('id'), on='id')
        links['mark'] = pd.concat([links['checks'] - links['frauds'] * fraudulous_weight])
        res = retweeters.join(links.set_index('id'), on='id')
        res['kind'] = kind
        return res
    elif kind == '<3':
        outCH = countLinks(likers, check, col_name='checks')
        outFR = countLinks(likers, fraud, col_name='frauds')
        links = outFR.join(outCH.set_index('id'), on='id')
        links['mark'] = pd.concat([links['checks'] - links['frauds'] * fraudulous_weight])
        res = likers.join(links.set_index('id'), on='id')
        res['kind'] = kind
        return res
    else:
        return pd.DataFrame.from_records([], columns=['empty'])


def theTweet(tweet_id):
    try:
        url = "https://twitter.com/twitter/statuses/" + str(tweet_id)
        api = "https://publish.twitter.com/oembed?url={}".format(url)
        response = requests.get(api)
        res = response.json()["html"]
        print(res)
        return res
    except:
        print(Utils.tweets_db[Utils.tweets_db['id'] == tweet_id][['text', 'media_url']].values[0])
        return "<blockquote class=\"twitter-tweet\"><p lang=\"fr\" dir=\"ltr\">" + \
               Utils.tweets_db[Utils.tweets_db['id'] == tweet_id]['text'].values[0] + "</p></blockquote>"


def extractData(data):
    data_lists = list(filter(lambda x: x is not None and len(x) > 1, data))
    data_idx = []

    for i in range(len(data_lists)):
        data_idx += [[i, data_lists[i]]]

    if len(data_idx) > 2:
        try:
            # tk = dict(data_idx[len(data_idx) - 2][1])['result_count']
            data_idx = data_idx[0:len(data_idx) - 2]
        except KeyError:
            data_idx = data_idx

    meta = list(filter(lambda x: x[0] % 2 == 1, data_idx))
    final = list(reduce(lambda x, y: x + y,
                        list(map(lambda x: x[1], list(filter(lambda x: x[0] % 2 == 0, data_idx))))))
    return [meta, final]


def extractFollowersList(username, token=None):
    user_id = Utils.client.get_user(username=username)
    followers = explore(Utils.client.get_users_followers, user_id.data.id, max_results=1000,
                        limit=100) if token is None else explore(Utils.client.get_users_followers, user_id.data.id,
                                                                 max_results=1000, limit=100,
                                                                 pagination_token=token)

    # replace with extractData(followers)
    followers_data = list(filter(lambda x: len(x) > 1, followers))
    followers_data_idx = []

    for i in range(len(followers_data)):
        followers_data_idx += [[i, followers_data[i]]]

    users_meta = list(filter(lambda x: x[0] % 2 == 1, followers_data_idx))
    users_final = list(reduce(lambda x, y: x + y,
                              list(map(lambda x: x[1], list(filter(lambda x: x[0] % 2 == 0, followers_data_idx))))))
    return {'meta': users_meta, 'users': users_final}


def users_df(users, account):
    df = pd.DataFrame.from_records(list(map(lambda u: [u['id'], u['username']], users['users'])),
                                   columns=['user_id', 'username'])
    df['account'] = account
    return df


def users_token_df(users, account):
    data = users['meta'][len(users['meta']) - 1]
    df = pd.DataFrame.from_records([[data[1]['result_count'], data[1]['previous_token']]],
                                   columns=['last_result_count', 'last_token'])
    df['account'] = account
    return df


def updateFollowersAccount(path, account):
    print('Working on', account)
    account_followers_path = path + 'followers/' + account + '_data.parquet'
    account_meta_path = path + 'meta/' + account + '_data.parquet'
    if not os.path.exists(account_followers_path):
        print(account, 'No data=> gathering it')
        users = extractFollowersList(account)
        udf = users_df(users, account)
        udf.to_parquet(account_followers_path)
        try:
            umeta = users_token_df(users, account)
            umeta.to_parquet(account_meta_path)
        except IndexError:
            print(account, "no meta data (to few followers)")
    else:
        if os.path.exists(account_meta_path):
            print(account, 'gathering data using last twitter token')
            token = pd.read_parquet(account_meta_path)['last_token'][0]
            users = extractFollowersList(account, token=token)
            udf = users_df(users, account)
            udf_old = pd.read_parquet(account_followers_path)
            udfs = pd.concat([udf, udf_old])
            udfs.drop_duplicates()
            udfs.to_parquet(account_followers_path)
            try:
                umeta = users_token_df(users, account)
                umeta.to_parquet(account_meta_path)
            except IndexError:
                print(account, "no meta data (to few followers)")
        else:
            print(account, 'data exists, but no saved token, gathering new data')
            users = extractFollowersList(account)
            udf = users_df(users, account)
            udf.to_parquet(account_followers_path)
            try:
                umeta = users_token_df(users, account)
                umeta.to_parquet(account_meta_path)
            except IndexError:
                print(account, "no meta data (to few followers)")


def updateFollowersDB(path):
    if not os.path.exists(path):
        print(path, 'does not exists. Creating it')
        os.mkdir(path)
    if not os.path.exists(path + 'followers'):
        print(path + 'followers', 'does not exists. Creating it')
        os.mkdir(path + 'followers')
    if not os.path.exists(path + 'meta'):
        print(path + 'meta', 'does not exists. Creating it')
        os.mkdir(path + 'meta')
    accounts = Utils.checker.union(Utils.fraudulous)
    cpt = 1
    for account in accounts:
        print(cpt, 'out of', len(accounts))
        updateFollowersAccount(path, account)
        cpt += 1


class Classification:
    classes = [0, 0, 0, 0, 0, 0]
    checked = [False, False, False, False, False, False]

    def __init__(self):
        self.classes = [0, 0, 0, 0, 0, 0]
        self.check = [False, False, False, False, False, False]

    def reset_classes(self, i):
        if i < len(self.classes):
            for j in range(0, len(self.classes) - 1):
                if j == i:
                    self.classes[j] = 1 if self.checked[j] else 0
                    self.checked[j] = not self.checked[j]
                else:
                    self.classes[j] = 0


def head(l: list) -> object:
    if l is not None:
        return l[0]
    else:
        return []


def tail(l: list) -> list:
    return l[1:len(l)]


def index(l: list, o: object) -> int:
    cpt = 0
    for item in l:
        if item == o:
            return cpt
        else:
            cpt += 1
