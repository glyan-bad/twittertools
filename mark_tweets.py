import os

import twitterTools
from twitterTools import *
from legacy_code.OpinEx import *
from legacy_code.legacy import *
import gradio as gr

if __name__ == '__main__':

    #Update the database of followers for the fraudulous and valuable accounts
    updateFollowersDB("/Users/glyan/workspace/twittos/")
    done = None

    # load already processed tweets in order to remove them from the list of to-do
    if os.path.exists(os.getcwd() + '/marks/') and len(os.listdir(os.getcwd() + '/marks/')) > 0:
        done = pd.read_parquet(os.getcwd() + '/marks/')
        done = done[['tweet_id']].drop_duplicates()
        done = list(done['tweet_id'])

    #loading pre-processed tweets
    tweets = pd.read_parquet("/Users/glyan/workspace/qts_rt-qt-ids")
    tweets = tweets[['rt_qt_id']].drop_duplicates()
    tweets = list(tweets['rt_qt_id'])
    print(len(tweets))

    # remove already processed tweets from the list of to-do
    if done is not None:
        tweets = [tweet for tweet in tweets if tweet not in done]
    print(len(tweets))

    # process tweets social marking
    for tweet in tweets:
        print("Processing tweet: https://twitter.com/twitter/statuses/%s" % str(tweet))
        data = None
        try:
            data = tweetSocialMark(tweet, "/Users/glyan/workspace/twittos/followers")
            data['tweet_id'] = tweet
            if len(data[data['id'] == -1]) == 2 and len(data) == 2:
                print("NO DATA FOR TWEET,", tweet)
            else:
                data.to_csv(os.getcwd() + '/marks/' + str(tweet) + '.csv')
        except:
            print("NO DATA FOR TWEET,", tweet)
