# updateFollowersDB("/Users/glyan/workspace/twittos/")

# input = st.text_input("Enter your tweet id")
# if input:

# dfTweets = tweetsDictToDF(tweets)
# print(tabulate(dfTweets, headers='keys', tablefmt='psql'))
# ids = sample(list(dfTweets['id']), 100)
# forEach(streamlitTest, ids)

# streamlitTest(1532318877769687041)

def getTxt(tweet):
    res = []
    ### text ###
    if 'extended_tweet' in tweet:
        res = [tweet['id'], tweet['created_at'], tweet['extended_tweet']['full_text']]
    else:
        res = [tweet['id'], tweet['created_at'], tweet['text']]
    if 'retweeted_status' in tweet and tweet['retweeted_status']['lang'] == 'fr':
        if 'extended_tweet' in tweet['retweeted_status']:
            res = [tweet['id'], tweet['created_at'], tweet['retweeted_status']['extended_tweet']['full_text']]
        else:
            res = [tweet['id'], tweet['created_at'], tweet['retweeted_status']['text']]
    ### quoted tweet ###
    if 'quoted_status' in tweet and tweet['quoted_status']['lang'] == 'fr':
        if 'extended_tweet' in tweet['quoted_status']:
            res = [tweet['id'], tweet['created_at'], tweet['quoted_status']['extended_tweet']['full_text']]
        else:
            res = [tweet['id'], tweet['created_at'], tweet['quoted_status']['text']]
    return res


query = 'NUPES lang:fr -is:retweet has:media'
tweets = Utils.client.search_all_tweets(end_time="2022-06-10T00:00:00Z", start_time="2021-06-10T00:00:00Z",
                                        query=query, tweet_fields=['context_annotations', 'created_at'],
                                        user_fields=Utils.user_fields,
                                        media_fields=Utils.media_fields,
                                        expansions=['attachments.media_keys', 'author_id'],
                                        max_results=100)

st.title("Tweet trustworthiness exploration")
inputTweet = st.text_input("Enter your tweet id")
if inputTweet:
    streamlitTest(inputTweet)

users = explore(Utils.client.get_users_followers, id.data.id, max_results=1000, limit=100)

"""
for id in ids:
    print("https://twitter.com/twitter/statuses/" + str(id))
    dfMedia = getMedia(tweets)
    # print(tabulate(dfMedia, headers='keys', tablefmt='psql'))

    dfRes = mergeTweetsMedia(dfTweets, dfMedia)
    # print(tabulate(dfRes, headers='keys', tablefmt='psql'))

    retweeters = getRetweeters(id)
    # retweeters = getRetweeters(1532318877769687041)
    # retweeters = getRetweeters(1535041044361904139)
    print(tabulate(retweeters, headers='keys', tablefmt='psql'))

    likers = getLikers(id)
    # likers = getLikers(1532318877769687041)
    print(tabulate(likers, headers='keys', tablefmt='psql'))

    quoters = getQuoters(id)
    # quoters = getQuoters(1534811568013201408)
    # quoters = getQuoters(1532318877769687041)
    print(tabulate(quoters, headers='keys', tablefmt='psql'))

# id = Utils.client.get_user(username='conspiration').data.id
# st.write(dfTweets)
# your_name = st.text_input("Enter your name")
# st.title(your_name)
# res = analyzeTweet(1532318877769687041)
# print(tabulate(res[0], headers='keys', tablefmt='psql'))
# print(tabulate(res[1], headers='keys', tablefmt='psql'))

"""
