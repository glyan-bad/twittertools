#!/usr/bin/python3

# coding: utf-8

#
# comment:

# Purpose:
#
# Comment:

# Code:

##########################################################################
#                            INITIALIZATION                              #
##########################################################################

import os, subprocess, codecs, sys, glob, re, getopt, random, operator, pickle
# import cPickle as pickle
from math import *

# import psyco
# psyco.full()
fnull = open(os.devnull, 'w')
# reload(sys)
# sys.setdefaultencoding('utf-8')


from collections import defaultdict

prg = sys.argv[0]


def P(output=''): input(output + "\nDebug point; Press ENTER to continue")


def Info(output='', ending='\n'):  # print(output, file=sys.stderr)
    sys.stderr.write(str(output) + ending)
    sys.stderr.flush()


DEBUG = 1
prefix = 'unspec.termex'


# ~ HOME = '/home/vincent'
# ~ if not os.path.exists(HOME):
# ~ HOME = '/udd/vclaveau'
# ~ if not os.path.exists(HOME): sys.exit('HOME global var is not set correctly')
# ~ else: Info('HOME is set to '+HOME)


def Mypickle_dump(file_name, obj):
    pickleFile = open(file_name, 'wb')
    pickle.dump(obj, pickleFile, pickle.HIGHEST_PROTOCOL)
    pickleFile.close()


def Mypickle_load(file2pickle):
    pickleFile = open(file2pickle, 'rb')
    obj = pickle.load(pickleFile)
    pickleFile.close()
    return obj


#######################################
# special imports


# gensim
# ~ sys.path.insert(1,HOME+'/stage/RI/ressources')
# ~ from gensim import corpora, models, similarities, matutils, utils
# ~ import TfidfmodelVC, BM25model, BM25model2, HellingerModel, HiemstraModel, mydocsim, mydocsim_old

from uuid import getnode as get_mac

from datetime import datetime, timezone
import time

import gzip, json

#######################################
# files

#######################################
# variables


T_MAC_ADDRESS = [0x0242AC190003, 0x0050569421E6, 0x6cc2177c8860]  # 0x000C298EC27A# 0x6cc2177c8860
T_UUID_ADDRESS = ['9fca21526bd6f05eea54e89e39444de69903b43ad7c66cb05ed675f330afc303',
                  'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
                  'a3340742b21e5354f1474406f60edd6a80f0599280b95f7ab1e82f01a23c9d7e']
CLIENTNAME = 'AVISTO (NexGenTV)'
UUID = ''

VERSION = '0.8'

TAGDIR = '/home/vincent/stage/util/TreeTagger'
HOME = '/home/vincent'
LEX_HOME = HOME + '/stage/Data/OpinionLexicons'


#########################################
# USAGE


################################################################################
################################################################################
##                                                                            ##
##                                 FUNCTIONS                                  ##
##                                                                            ##
################################################################################
################################################################################


def get_uuid():
    p = subprocess.Popen('/sbin/blkid | grep -oP \'UUID="\\\K[^"]+\' | sha256sum | awk \'{print $1}\'', shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return p.communicate()[0].decode(encoding='UTF-8').rstrip('\r\n')


def Warn(output='', debug=DEBUG):
    if debug > 0:
        sys.stderr.write(str(output) + '\n')


def ReadPickle(file_BD):
    h_BD = {}
    if file_BD is not None and os.path.exists(file_BD):
        pickleFile = open(file_BD, 'rb')
        h_BD = pickle.load(pickleFile)
        pickleFile.close()
    else:
        Warn('WARNING: no (existing) DB file; will use simple TF')

    return h_BD


def Tokenize(txt):
    print(txt)

    txt = re.sub(' (d|j|l|m|n|s|t|qu)[\'’]', ' \g<1>\' ', ' ' + txt.lower() + ' ')
    txt = re.sub('-t?-?(je|me|tu|il|elle|on|nous|vous|ils|elles) ', ' \g<1> ', ' ' + txt.lower() + ' ')

    # normalisation
    t_doc = []
    for w in re.split('[ ",;:\(\)\n\.!\?…]+', txt.strip(' ')):
        w = re.sub('ä|à|á|â', 'a', w)
        w = re.sub('ë|è|é|ê', 'e', w)
        w = re.sub('ï|ì|í|î', 'i', w)
        w = re.sub('ö|ò|ó|ô', 'o', w)
        w = re.sub('ü|ù|ú|û', 'u', w)
        w = re.sub('aa+', 'a', w)
        w = re.sub('ee+', 'e', w)
        w = re.sub('ii+', 'i', w)
        w = re.sub('ooo+', 'o', w)
        w = re.sub('uu+', 'u', w)
        t_doc.append(w)

    return t_doc


#####################################
# actants : joueurs du match, hommes politiques...

def Read_actants(file_actants, bool_include_type=True):
    h_regexp_actants = {}
    for line in codecs.open(file_actants, 'r', 'utf-8'):
        t_l = line.rstrip('\r\n').split('\t')
        if not len(t_l) == 2: continue
        type_pers = t_l[0]
        norm = t_l[1].split(',')[0]  # first of list is the normalized version
        t_l = ['#?' + re.sub(' ', ' #?', e) for e in t_l[1].split(',') if e not in ['']]

        if len(t_l) > 0:
            ma_re = r'(^|[ ,;:.!?…"\'\n])(' + '|'.join(t_l) + ')([ ",;:?!.…\n])'
            if bool_include_type:
                h_regexp_actants[' ' + type_pers + '_' + norm + ' '] = re.compile(ma_re, flags=re.I)
            else:
                h_regexp_actants[' ' + norm + ' '] = re.compile(ma_re, flags=re.I)

    # print(h_regexp_actants[' politic_Emmanuel_Macron '])
    return h_regexp_actants


#########################################
# opinion lexicons


def Read_lexicons(lang, file_dict_lexicon):
    Info('Getting pre-trained polarity information')

    if file_dict_lexicon is not None and not file_dict_lexicon == 'redo' and os.path.exists(file_dict_lexicon):
        h_lexicon = ReadPickle(file_dict_lexicon)

    else:

        file_noun, file_adj, file_verb = LEX_HOME + '/LIDILEM/Noms.csv', LEX_HOME + '/LIDILEM/Adjectifs.csv', LEX_HOME + '/LIDILEM/Verbes.csv'

        h_lexicon = {'N': {}, 'V': {}, 'ADJ': {}, 'ADV': {}, }

        for line in codecs.open(file_adj, 'r', 'utf-8'):
            wf, domaine, sousdomaine, _, _, _, _, polarite = line.rstrip('\r\n').split('\t')
            domaine = re.sub('[/\+]+', ' ', domaine)
            sousdomaine = re.sub('[/\+]+', ' ', sousdomaine)
            if polarite == 'positif/négatif': polarite = 'ambigu'
            h_lexicon['ADJ'][wf] = [domaine, sousdomaine, polarite]
        for l in 'beau belle efficace époustouflant excellent fier généreux gentil habile hors_du_commun humaniste illustre irréprochable joli magnifique meilleur mignon objectif préféré raffiné réaliste réussi solidaire sublime super'.split(
                ' '):
            h_lexicon['ADJ'][l] = ['', '', 'positif']
        for l in 'abêtissant apocalyptique bizarre contradictoire critiqué dangereux débile énervé faciste has_been hypocrite illégal inapplicable incompétent inexistant infâme influençable inutile iresponsable grave grotesque inaudible insoutenable irresponsable lamentable loufoque maigre mal_à_l\'aise maladif mauvais merdeux mort narcissique nul paranoïaque pourri populiste raciste ridicule scandalisé sexiste'.split(
                ' '):
            h_lexicon['ADJ'][l] = ['', '', 'négatif']

        h_verb = defaultdict(lambda: ['', '', ''])
        for line in codecs.open(file_verb, 'r', 'utf-8'):
            wf, domaine, sousdomaine, _, _, _, _, _, polarite = line.rstrip('\r\n').split('\t')
            domaine = re.sub('[/\+]+', ' ', domaine)
            sousdomaine = re.sub('[/\+]+', ' ', sousdomaine)
            if polarite == 'positif/négatif': polarite = 'ambigu'
            h_lexicon['V'][wf] = [domaine, sousdomaine, polarite]
        for l in 'adorer bénéficier émouvoir gagner kiffer marquer rire'.split(' '):
            h_lexicon['V'][l] = ['', '', 'positif']
        for l in 'accuser bombarder chier clasher convoquer compliquer crever critiquer dégager démissionner enterrer insulter insurger menacer mentir moquer mourir pavaner perdre ridiculiser risquer saboter scandaliser sucrer tacler taire tomber truquer vanner violer'.split(
                ' '):
            h_lexicon['V'][l] = ['', '', 'négatif']

        for line in codecs.open(file_noun, 'r', 'utf-8'):
            wf, domaine, sousdomaine, _, _, polarite, _ = line.rstrip('\r\n').split('\t')
            domaine = re.sub('[/\+]+', ' ', domaine)
            sousdomaine = re.sub('[/\+]+', ' ', sousdomaine)
            if polarite == 'positif/négatif': polarite = 'ambigu'
            h_lexicon['N'][wf] = [domaine, sousdomaine, polarite]
        for l in 'bonus boss caviar charme espoir gagnant génie hassenette honneur leader mérite phare sens_interdit'.split(
                ' '):
            h_lexicon['N'][l] = ['', '', 'positif']
        for l in 'abattoir absurdité amalgame amateur amateurisme attentat attentisme bassesse bêtise bobard bordel cacophonie calomnie caudillo chômage clown complice con connerie contradiction convocation crucifixion cynisme danger défaite démagogie désaveu dictateur dommage empoisonnement énergumène erreur exaspération exil facho feignasse fraudeur galère génocide grotesque guerre gueule hagra hypocrisie hypocrite incapable incapacité inconscient incurie indécence inefficacité inexpérience injustice islamiste kamikaze ko luxure macho mafia magouille magouilleur malhonnêteté manipulateur manque mensonge menace mépris merdeux menteur meurtre meurtrier mort négligence n_importe_quoi ordure partialité pénurie polémique pollution problème procès propagande prostitution racisme raciste ramassis récession ripoux salopard saloperie sanction sbire scandale sexisme sexiste surenchère terrorisme toquard traîtrise traquenard trouillard répression usurpation viol'.split(
                ' '):
            h_lexicon['N'][l] = ['', '', 'négatif']

        for l in 'agréablement heureusement joyeusement'.split(' '):
            h_lexicon['ADV'][l] = ['', '', 'positif']
        for l in 'cyniquement désagréablement malheureusement'.split(' '):
            h_lexicon['ADV'][l] = ['', '', 'négatif']

        #############################################################################################
        # ANEW FR
        file_anew = LEX_HOME + '/ANEW 2010/ANEW2010All_fr.txt'
        h_lexicon['ANEW'] = {}
        with codecs.open(file_anew, 'r', 'utf-8') as f_anew:
            next(f_anew)
            for l in f_anew:
                t_l = l.rstrip('\r\n').split('\t')
                wf, _, _, pol, _, aro, _, dom, _ = t_l
                h_lexicon['ANEW'][wf] = [float(pol), float(aro), float(dom)]

        #############################################################################################
        # FEEL
        file_feel = LEX_HOME + '/FEEL/FEEL.csv'
        h_lexicon['FEEL'] = {}
        with  codecs.open(file_feel, 'r', 'utf-8') as f_feel:
            next(f_feel)
            for l in f_feel:
                _, lem, pol, joy, fear, sadness, anger, surprise, disgust = l.rstrip('\r\n').split(';')
                lem = lem.replace(' ', '_')
                joy, fear, sadness, anger, surprise, disgust = [int(x) for x in
                                                                (joy, fear, sadness, anger, surprise, disgust)]
                if joy + fear + sadness + anger + surprise + disgust == 0: polarity = 'neutral'
                h_lexicon['FEEL'][lem] = [pol] + [int(x) for x in (joy, fear, sadness, anger, surprise, disgust)]

        ######################################################################
        # un peu de nettoyage des lexiques (propre au corpus)
        for w in ['grave', 'putain', 'seul']:
            h_lexicon['ADJ'].pop(w, None)
            h_lexicon['ANEW'].pop(w, None)
            h_lexicon['FEEL'].pop(w, None)
        for w in ['idée', 'lit', 'maladie', 'partie', 'putain', 'seul', 'virus']:
            h_lexicon['N'].pop(w, None)
            h_lexicon['ANEW'].pop(w, None)
            h_lexicon['FEEL'].pop(w, None)
        for w in ['comprendre', 'garder', 'quitter']:
            h_lexicon['V'].pop(w, None)
            h_lexicon['ANEW'].pop(w, None)
            h_lexicon['FEEL'].pop(w, None)

        ########################################################################
        # locutions, expressions
        h_lexicon['re_pos'] = re.compile('(' + '|'.join(
            ['a totalement raison', 'a raison', 'donner raison', 'entièrement d\'accord', 'gagner la guerre',
             'Lueur d\'espoir', 'Pas si bête', 'total soutien', 'Très juste réflexion', 'trop tard']) + ')')
        h_lexicon['re_neg'] = re.compile('(' + '|'.join(
            ['aucune crédibilité', 'coup de gueule', 'Honte à vous', 'il rame', 'le prix du sang', 'mal au coeur',
             'pas trop mal', 'Pauvre France', 'pauvre type', 'politique politicienne', 'vous faire crever',
             'vous vous moquez']) + ')')

        Mypickle_dump(file_dict_lexicon, h_lexicon)

    return h_lexicon


##############################################################################


# 💩🤬❌😡🖕


##############################################################################
##############################################################################


################################################################################
################################################################################
##                                                                            ##
##                                   MAIN                                     ##
##                                                                            ##
################################################################################
################################################################################


class TweetArchiveReader(object):

    def __init__(self, dir_prefix, percent_kept_total=100, percent_kept_fr=100, percent_kept_rt=100,
                 percent_kept_qt=100):
        self.dir_prefix = dir_prefix
        self.percent_kept_total = percent_kept_total
        self.percent_kept_fr = percent_kept_fr
        self.percent_kept_rt = percent_kept_rt
        self.percent_kept_qt = percent_kept_qt

    def GetTimestamp(self, tweet):
        if False and 'timestamp_ms' in tweet:
            ts = int(int(tweet['timestamp_ms']) / 1000)
        else:
            # expectinga LC_TIME set to en_US.UTF-8
            date = datetime.strptime(tweet["created_at"], '%a %b %d %H:%M:%S %z %Y').replace(
                tzinfo=timezone.utc).astimezone(tz=None)  # .strftime('%Y-%m-%d %H:%M:%S')
            ts = int(datetime.timestamp(date))
        return ts

    def GetText(self, tweet):

        text, qt_text, rt_text = '', '', ''
        ### text ###
        if 'extended_tweet' in tweet:
            text = tweet['extended_tweet']['full_text']
        else:
            text = tweet['text']

        ### retweet ###
        if 'retweeted_status' in tweet and tweet['retweeted_status']['lang'] == 'fr':
            if random.randint(0, 100) > self.percent_kept_rt:
                return '', '', ''

            if 'extended_tweet' in tweet['retweeted_status']:
                rt_text = tweet['retweeted_status']['extended_tweet']['full_text']
            else:
                rt_text = tweet['retweeted_status']['text']
            text = ''

        ### quoted tweet ###
        if 'quoted_status' in tweet and tweet['quoted_status']['lang'] == 'fr':
            if random.randint(0, 100) > self.percent_kept_qt:
                return '', '', ''

            if 'extended_tweet' in tweet['quoted_status']:
                qt_text += '\n' + tweet['quoted_status']['extended_tweet']['full_text'] + '.'
            else:
                qt_text += '\n' + tweet['quoted_status']['text'] + '.'
        # if '…' in text_tmp:
        # pprint(tweet)
        #    print(text_tmp)
        #    P()
        if len(qt_text) > 0 and len(rt_text) == 0 and len(text) == 0:
            rt_text = qt_text
            qt_text = ''

        return text.strip('\r\n'), qt_text.strip('\r\n'), rt_text.strip('\r\n')

    def __iter__(self):

        for dir in sorted(glob.glob(self.dir_prefix + '*')):
            Info('\nProcessing dir ' + dir)
            for fn_gz in sorted(glob.glob(dir + '/*.jsonl.gz')):  # ,key=lambda x: os.path.getctime(x)):
                Info('Processing file ' + fn_gz, ending='\n')

                try:
                    with gzip.open(fn_gz, 'rt', encoding='utf-8', errors='ignore') as fn_jsonl:
                        for line_nb, l_tweet in enumerate(fn_jsonl):
                            # for line_nb,l_tweet in enumerate(fn_jsonl.read().splitlines()):
                            try:
                                if l_tweet == '' or random.randint(0, 1000) > 10 * self.percent_kept_total: continue

                                tweet = json.loads(l_tweet)
                                # tweet = orjson.loads(l_tweet) # is there a simpler way?
                                if tweet['lang'] == 'fr' and random.randint(0, 1000) < 10 * self.percent_kept_fr:
                                    yield tweet

                            except ValueError as error:
                                print('pb in file/line ', fn_gz, line_nb)
                                print(error)
                                pass
                except OSError as error:  # CRC check failed or any OS related error
                    print('*********** pb when processing ' + fn_gz)
                    print(error)
                    P()

    def Iter_text(self, bool_text=False):
        for dir in sorted(glob.glob(args.dir_prefix + '*')):
            Info('\nProcessing dir ' + dir)
            for fn_gz in sorted(glob.glob(dir + '/*.jsonl.gz')):  # ,key=lambda x: os.path.getctime(x)):
                Info('Processing file ' + fn_gz, ending='\n')

                try:
                    with gzip.open(fn_gz, 'rt', encoding='utf-8', errors='ignore') as fn_jsonl:
                        for line_nb, l_tweet in enumerate(fn_jsonl.read().split('\n')):
                            try:
                                if l_tweet == '': continue

                                tweet = dict(json.loads(l_tweet))  # is there a simpler way?
                                if tweet['lang'] == 'fr' and random.randint(0, 1000) < 10 * self.percent_kept_fr:
                                    text, qt_text, rt_text = self.GetText(tweet)
                                    if text + qt_text + rt_text == '': continue
                                    # if 'Agnès Buzyn' in text_tmp:
                                    #    print(text_tmp)
                                    #    P()
                                    # pprint(tweet)
                                    # P()
                                    ts = GetTimestamp(tweet)

                                    yield ts, text, qt_text, rt_text, tweet

                            except ValueError as error:
                                print('pb in file/line ', fn_gz, line_nb)
                                print(error)
                                pass
                except OSError as error:  # CRC check failed or any OS related error
                    print('*********** pb when processing ' + fn_gz)
                    print(error)
                    P()


class OpinEx(object):

    def Process_html(self, txt):
        txt = txt.replace('&nbsp;', ' ')
        txt = txt.replace('&quot;', '"')
        txt = txt.replace('&gt;', '>')
        txt = txt.replace('&lt;', '<')
        txt = txt.replace('&amp;', ' et ')
        # txt = txt.replace('\\46\#','&#')
        txt = txt.replace('&#39;', '\'')
        txt = txt.replace('&#8211;', '-')
        txt = txt.replace('&#8217;', '\'')
        txt = txt.replace('&#8216;', '\'')
        # txt = re.sub('&a[^;]{,6};','a',txt)
        # txt = re.sub('&e[^;]{,6};','e',txt)
        # txt = re.sub('&i[^;]{,6};','i',txt)
        # txt = re.sub('&o[^;]{,6};','o',txt)
        # txt = re.sub('&u[^;]{,6};','u',txt)

        txt = txt.replace('&agrave;', 'à')
        txt = txt.replace('&eacute;', 'é')
        txt = txt.replace('&egrave;', 'è')
        txt = txt.replace('&aacute;', 'á')
        txt = txt.replace('&acirc;', 'â')
        txt = txt.replace('&atilde;', 'ã')
        txt = txt.replace('&auml;', 'ä')
        txt = txt.replace('&aring;', 'a')
        txt = txt.replace('&aelig;', 'ae')

        txt = txt.replace('&ccedil;', 'ç')

        txt = txt.replace('&ecirc;', 'ê')
        txt = txt.replace('&euml;', 'ë')
        txt = txt.replace('&eacute;', 'é')
        txt = txt.replace('&egrave;', 'è')

        txt = txt.replace('&igrave;', 'ì')
        txt = txt.replace('&iacute;', 'í')
        txt = txt.replace('&Iacute;', 'Í')
        txt = txt.replace('&icirc;', 'î')
        txt = txt.replace('&iuml;', 'ï')

        txt = txt.replace('&eth;', 'ð')
        txt = txt.replace('&Ntilde;', 'Ñ')
        txt = txt.replace('&ntilde;', 'ñ')

        txt = txt.replace('&ograve;', 'ò')
        txt = txt.replace('&oacute;', 'ó')
        txt = txt.replace('&Oacute;', 'Ó')
        txt = txt.replace('&ocirc;', 'ô')
        txt = txt.replace('&otilde;', 'õ')
        txt = txt.replace('&ouml;', 'ö')
        txt = txt.replace('&oslash;', 'o')

        txt = txt.replace('&ugrave;', 'ù')
        txt = txt.replace('&uacute;', 'ú')
        txt = txt.replace('&ucirc;', 'û')
        txt = txt.replace('&uuml;', 'ü')

        txt = txt.replace('&yacute;', 'ỳ')
        txt = txt.replace('&thorn;', 't')
        txt = txt.replace('&yuml;', 'ÿ')

        txt = re.sub('&a[^ ;]{,6};', 'a', txt)
        txt = re.sub('&e[^ ;]{,6};', 'e', txt)
        txt = re.sub('&i[^ ;]{,6};', 'i', txt)
        txt = re.sub('&o[^ ;]{,6};', 'o', txt)
        txt = re.sub('&u[^ ;]{,6};', 'u', txt)

        txt = re.sub('&[^ ;]{,7};', ' ', txt)

        return txt

    def Expand_short_URL(self, short_url):

        if short_url in self.h_short2long: return self.h_short2long[short_url]

        try:
            long_url = requests.head(short_url, allow_redirects=True).url
        except:
            long_url = 'unknown'

        self.h_short2long[short_url] = long_url
        return long_url

    def Extract_URL(self, text, unshorten=True):
        t_url = []
        for m in re.finditer('(https?://[^ \n\r…]+)', text):
            Info('.', ending='')
            url = m.group(1)
            if unshorten: url = self.Expand_short_URL(url)
            if not url == 'unknown':
                url = re.sub('https?://(www\.)?', '', url)
                url = re.sub('^(?:rss|mobile|m|videos?)\.', '', url)
                url = re.sub('/.*', '', url)
                t_url.append(url)

        return t_url

    def Reaccentue(self, w):

        if len(w) < 4: return w
        if self.h_BD is None: return w
        if re.search('â|é|è|ê|ë|î|ï|ô|ù|û', w) is not None: return w
        if re.search('e', w) is None: return w

        h_variants = {w: 1}
        h_accents = {'e': ['e', 'é', 'è', 'ê', 'ë']}

        t_pos = [i for i, l in enumerate(w) if l == 'e']
        for pos in t_pos:
            t_variants = list(h_variants.keys())
            for variant in t_variants:

                if pos == 0:
                    before = ''
                else:
                    before = variant[0:pos]

                if pos == len(w):
                    after = ''
                else:
                    after = variant[pos + 1:]

                for e in h_accents['e']:
                    h_variants[before + e + after] = 1

        best = max(h_variants, key=lambda v: self.h_BD.get(v, 0))
        if best not in self.h_BD: return w

        return best

    def PreProcessTweet(self, l, bool_remove_citation=False, bool_remove_neg=False, bool_remove_duration=False,
                        bool_normalize_polarity=False, bool_normalize_punct=False, bool_remove_URL=True,
                        bool_replace_smileys=True):
        l = ' ' + l.rstrip('\r\n') + ' '

        l = l.replace('\u2069', ' ')
        # les URL
        if bool_remove_URL:
            l = re.sub('http[^ ]+', ' ', l)
        else:
            l = re.sub('http[^ ]+', ' URL . ', l)
        # les 1/2
        l = re.sub(' +[0-9]+/[0-9]+ *$', ' ', l)

        l = re.sub('["«»“”]+', '"', l)
        # citation
        if bool_remove_citation:
            l = re.sub('[«"“]([^"”» ]+)["”»]', ' \g<1> ', l)  # pas juste un mot
            l = re.sub('^ *[«"“]([^"”»]+)["”»] *$', ' \g<1> ', l)  # pas tout le tweet
            l = re.sub('(\'\'|[«"“])[^"”»]+ [^"”»]+(\'\'|["”»])', ' CITATION ', l)

        l = re.sub('[ ™®©]+', ' ', l)
        l = re.sub('@@', '@', l)
        l = re.sub('\.?@', ' @', l)

        # voyelles repetees
        l = re.sub('aa{2,}', 'a', l, flags=re.I)
        l = re.sub('ee{2,}', 'e', l, flags=re.I)
        l = re.sub('ii{2,}', 'i', l, flags=re.I)
        l = re.sub('oo{3,}', 'o', l, flags=re.I)
        l = re.sub('uu{2,}', 'u', l, flags=re.I)
        l = re.sub('yy{2,}', 'y', l, flags=re.I)

        l = re.sub('œ', 'oe', l, flags=re.I)

        # @Zehub|@zehub|@scoopit
        l = re.sub(' *(VIA|via|RT|Rt|▶️+|►+|>+|cc|=+>+) ', ' via ', l)
        # l = re.sub('\(? *(VIA|via|RT|►+|>+|\|)( :)? Journal *\)?',' cite_journal ',l)
        # l = re.sub('via Journal *$',' cite_journal ',l)
        # l = re.sub(' Journal( URL)* *$',' cite_journal ',l)
        # l = re.sub('via +@[^ :]+(?: :|:)?',' cite_pers ',l)

        l = re.sub(' [Ee]n ?train d[\'e]', ' en_train_de ', l)

        l = re.sub(' svp ', ' ', l)

        l = l.replace('#', ' #')
        # plusieurs hashtag -> pas une phrase
        l = re.sub('(#[^ \n]+) +(#[^ \n]+)', '\g<1> . \g<2> . ', l)
        l = re.sub('(#[^ \n]+) +(#[^ \n]+)', '\g<1> . \g<2> . ', l)

        # mots-dièses
        def SplitHash(m):  # on coupe sur les majuscules sauf si que des majuscules
            if re.match('^[A-Z]+$', m.group(1)) is not None: return m.group(1).lower()
            return re.sub('([a-zéè])([A-Z])', '\g<1> '.lower() + '\g<2>'.lower(), m.group(1))

        l = re.sub('#([^ ]+)', SplitHash, l)
        # l = re.sub('#',' ',l)

        # les actants ##########################################
        for actant in self.h_actants:
            l = self.h_actants[actant].sub('\g<1> ' + actant + ' \g<3>', l)

        # reponse a q'un
        # l = re.sub('^ *"?Pers','RéponsePers',l)

        # les smileys #####################################
        if bool_replace_smileys:
            l = re.sub('😳|:o|oua+h+ou+|😱|😮|&#x1f62e;| omg ', ' MonOMG ', l, flags=re.I)
            l = re.sub(
                '😍|😻|❤|😍|💏|💋|💛|💜|💓|💕|💖|💟|💗|💘﻿|<3|&lt;3|&?#x1f60d;|&?#x1f61a;|&?#x1f618;|&?#x2764;️|&?#x1f444;',
                ' MonAmour ', l)
            l = re.sub('🙏', ' merci ', l)
            l = re.sub(
                '\^\^|ツ|☺|😂|&#x1f602;|💃|😈|😸|😹|😊|😄|😅|😁|😏|😉|&#x1f60a;|&#x1f603;|&#x1f60e;|:-?[\)DPp]+|xD+|md+r+|LO+L|md+r+|P+T+D+R+|p+t+d+r+|you+p+i+|a?ha(?:ha)+h?|hi(?:hi)+|mou(ah)+a?|mou(ha)+h?|gorafi',
                ' lol ', l, flags=re.I)
            l = re.sub(':\'?-?\(+|-_-\'?|:-?/|😢|😪|😧|😨|🙀|😳|😡|😞|😤|&#x1f62b;|&#x1f622;|#PasCool|pff+|tss+|#vdm',
                       ' paslol ', l, flags=re.I)
            # exclus 😭 car trop souvent confondu avec tear of joy
            l = re.sub('😐|😬', ' paslol ', l, flags=re.I)
            l = re.sub('😜|😉|;-?[\)Dp]+|&#x1f609;|&#x1f61c;|&#x1f600;|&#x1f619;', ' winking ', l, flags=re.I)
            l = re.sub('&#x1f644;|🙄', ' rolling ', l)  # face with rolling eyes

            l = re.sub('🤬| #?[Gg]+r+ ', ' colère ', l)
            l = re.sub('[♫♪]+', ' symbmusique ', l)
            l = re.sub('\\\\o/|🙌|👏|👌|&#x1f48e;|&#x1f44d;|&#x270c;|&#x1f44c;|yes+', ' bravo ', l, flags=re.I)
            # 😴|😔|😒|
            l = l.replace('&#x1f60c;', ' . soulagé . ')

            l = l.replace('&#x1f354;', ' burger ')
            l = l.replace('la 💀', 'la mort')

            l = l.replace('🇺🇸', ' États-Unis ')
            l = l.replace('🇨🇳', ' Chine ')
            l = l.replace('🇫🇷', ' France ')
            l = l.replace('🇮🇹', ' Italie ')
        # HTML characters
        l = self.Process_html(l)

        # ponctuations
        l = re.sub('\n', ' .  ', l)
        if bool_normalize_punct:
            l = re.sub('[!\?](?: *[!\?])+', ' MonExclaQuestion .  ', l)
            l = re.sub('!(?: *!)+', ' MonExclamation .  ', l)
            l = re.sub('\?(?: *\?)+', ' MonQuestion .  ', l)
        l = re.sub('\.(?: *\.)+', ' …  ', l)
        # ~ l = re.sub('…',' etc .  ',l)
        l = re.sub('([0-9]).([0-9]{3})', '\g<1>\g<2>', l)  # 100.000 -> 100000
        l = re.sub('([\.,;\?!:])', ' \g<1> ', l)

        l = re.sub('[´’’]', '\'', l)
        l = l.replace('"+', ' ')

        l = re.sub('\[[0-9]+/[0-9]+\]', '', l)
        l = re.sub('^\[(.+)\]', '\g<1> .', l)

        l = l.replace(' & ', ' et ')
        l = l.replace(' &', ' et ')
        l = re.sub('([0-9])&([0-9])', '\g<1> et \g<2>', l)
        l = re.sub(' -([0-9,\.]+%)', ' moins \g<1>', l)
        l = re.sub('^-([0-9,\.]+%)', 'moins \g<1>', l)
        l = re.sub(' \$([0-9,\.]+M?) ', ' \g<1> d\'euros ', l)
        l = re.sub('([▪•·▶️►>⚠]+| [:\-] )', ' : ', l)
        l = re.sub('([:,;!\?])([a-zA-ZéÉ])', '\g<1> \g<2>', l)
        l = l.replace('/', ' ou ')
        l = re.sub('', 'oe', l)
        l = re.sub('€|¥|\$|DH ', ' euros ', l)
        l = re.sub('dollars?', ' euros ', l)

        if bool_remove_duration:
            # heures
            l = re.sub(' ([0-9]+) ?mns? ', ' MonDuree ', l)
            l = re.sub(' ([0-9]+) ?heures? ', ' MonDuree ', l)
            l = re.sub(' ([0-9]+)[:Hh]([0-9]+) ', ' MonHoraire ', l)
            l = re.sub(' ([0-9]+)[Hh] ', ' MonHoraire ', l)
            l = re.sub(' ([0-9]+) (heures?|minutes?|secondes?) ', ' MonHoraire ', l)

        # on met une espace avant-après les ponctuations
        l = re.sub('([",;:\(\)\n\.!\?…])', ' \g<1> ', l)

        l = re.sub(' 1[èe]res? ', ' première ', l)
        l = re.sub(' 1er?s? ', ' premier ', l)
        l = re.sub(' 2nde ', ' seconde ', l)
        l = re.sub(' 1 ', ' un ', l)
        l = re.sub(' 1([a-z]+)', ' un \g<1>', l)
        l = re.sub(' ([0-9]+)([a-z]+)', ' \g<1> \g<2>', l)
        l = re.sub('fait flipper ', 'fait peur ', l)
        l = re.sub(' flippe ', ' ai peur ', l)
        l = re.sub('Fichtre ', 'Surprenant ', l)
        l = re.sub('([abcdefghijklmnopqrstuvwxyz])\1{2,}', '\g<1>', l)
        l = re.sub('^J([^A-Zaeiouy])', 'Je \g<1>', l)
        # l = re.sub(' E([^n])',' É\g<1>',l)
        # l = re.sub('[\'’]',"' ",l)
        # l = re.sub('([;,:!])',' \g<1> ',l)
        # l = re.sub('([\.]) ',' \g<1> ',l)
        l = l.replace(' a avoir ', ' à avoir ')
        l = l.replace(' a être ', ' à être ')
        l = re.sub(' a ([^ ]er) ', ' à \g<1> ', l)
        l = l.replace(' avc ', ' avec ')
        l = l.replace(' aprèsplus ', ' après plus ')
        l = l.replace(' [Aa]prèsle ', ' après le ')
        l = re.sub(' [Aa]uj. ', ' aujourd\'hui ', l)  # ,flags=re.I)
        l = re.sub(' [Bb]cp ', ' beaucoup ', l)
        l = re.sub(' c ?est ', ' c\'est ', l)  # ,flags=re.I)
        l = re.sub(' C ?EST ', ' C\'EST ', l)
        l = re.sub(' C ', ' c\'est ', l, flags=re.I)
        l = re.sub('ceniveau ', ' ce niveau ', l, flags=re.I)
        l = l.replace(' Ca ', ' ça ')
        l = l.replace(' ca ', ' ça ')
        l = re.sub(' gt ', ' j\'étais ', l, flags=re.I)
        l = re.sub(' ct ', ' c\'était ', l, flags=re.I)
        l = re.sub(' cmt ', ' comment ', l, flags=re.I)
        l = re.sub(' (d|l|j|m|s|t|qu)\'', ' \g<1>e ', l, flags=re.I)
        l = re.sub(' C-[aà]-dire ', ' c\'est-à-dire ', l)
        l = re.sub(' c[aà]d ', ' c\'est-à-dire ', l)
        l = re.sub(' chais ', ' je sais ', l, flags=re.I)
        l = re.sub(' cheum ', ' méchant ', l, flags=re.I)
        l = re.sub(' [Cc]hq ', ' chaque ', l)
        l = re.sub(' Chui?s? ', ' . Je suis ', l)
        l = re.sub(' chui?s? ', ' je suis ', l)
        l = l.replace(' comm\' ', ' communication ')
        l = l.replace(' coute ', ' coûte ')
        l = l.replace(' coutent ', ' coûtent ')
        l = re.sub(' [Cc]huis? ', ' je suis ', l)

        l = l.replace(' ds ', ' dans ')
        l = l.replace(' dsl ', ' désolé ')
        l = re.sub(' degueux? ', ' dégueulasse ', l)
        l = re.sub(' d[eé]j[aà] ', ' déjà ', l)
        l = re.sub(' de ouf+ ', ' superbe ', l)
        l = l.replace(' de fou ', ' superbe ')
        l = l.replace(' dés mariages ', ' des mariages ')
        l = re.sub(' d\'?la ', ' de la ', l)
        l = re.sub(' d\'?le ', ' de le ', l)
        l = re.sub('Doncvous ', ' Donc vous ', l)
        l = l.replace('economie ', 'économie ')
        l = l.replace('Economie ', 'Économie ')
        l = re.sub(' [Ff]aque ', ' ainsi ', l)
        l = l.replace(' frchmt ', ' franchement ')
        l = l.replace(' g ', ' j\'ai ')
        l = l.replace(' gvt ', ' gouvernement ')
        l = re.sub(' hu*m+ ', ' hum ', l, flags=re.I)
        l = re.sub(' ITW ', ' interview ', l, flags=re.I)
        l = re.sub(' irak ', ' Irak ', l)
        l = re.sub('(?:^|[ .,!?])ily ', ' il y ', l, flags=re.I)
        l = re.sub(' ilest ', ' il est ', l, flags=re.I)
        l = re.sub(' iln\'y ', ' il n\'y ', l, flags=re.I)
        l = re.sub(' imbecile', ' imbécile', l, flags=re.I)
        l = re.sub(' j\'?veu[xt]?', ' je veux ', l, flags=re.I)
        l = re.sub(' j\'?suis', ' je suis ', l, flags=re.I)
        l = re.sub(' j\'?sais', ' je sais ', l, flags=re.I)
        l = re.sub(' jss', ' je suis ', l, flags=re.I)
        l = re.sub(' j ', ' j\'', l)
        l = re.sub(' [Jj]\'?([bcdfghjklmnpqrstvwxz])', ' je \g<1>', l)
        l = re.sub(' jai ', ' j\'ai ', l)
        l = re.sub(' jen ', ' j\'en ', l)
        l = re.sub(' jesp[èe]re ', ' j\'espère ', l)
        l = re.sub(' kala[sc]h ', ' kalashnikov ', l)
        l = l.replace(' ke ', ' que ')
        l = l.replace(' ki ', ' qui ')
        l = re.sub(' k\'', ' qu\'', l)
        l = re.sub(' lgtps? ', ' longtemps ', l)
        l = re.sub(' m[iy]th?o ', ' menteur ', l)
        l = re.sub(' [Mm]skn ', ' meskine ', l)
        l = re.sub(' mtn ', ' maintenant ', l)
        l = l.replace(' mm ', ' même ')
        l = re.sub(' m[eè]me ', ' même ', l)
        l = re.sub(' [Mm]\'?([bcdfghjklmnpqrstvwxz])', ' me \g<1>', l)
        l = l.replace(' n°', ' numéro ')
        l = l.replace(' nan ', ' non ')
        l = l.replace(' nrv ', ' énerver ')
        l = re.sub(' oklm ', ' au_calme ', l, flags=re.I)
        l = l.replace(' pbl ', ' problème ')
        l = re.sub(' p\'?tit ', ' petit ', l)
        l = re.sub(' pn?dn?t ', ' pendant ', l)
        l = l.replace(' ptetre ', ' peut-être ')
        l = l.replace(' paske ', ' parce que ')
        l = re.sub(' pc[qk] ', ' parce que ', l)
        l = re.sub(' [Pp][cs][qk] ', ' parce que ', l)
        l = re.sub(' [Pp][kq] ', ' pourquoi ', l)
        l = re.sub(' [Pp]koi ', ' pourquoi ', l)
        l = l.replace(' parano ', ' paranoïaque ')
        l = re.sub('pandemie', 'pandémie', l, flags=re.I)
        l = l.replace(' pk ', ' pourquoi ')
        l = l.replace(' plait ', ' plaît ')
        l = l.replace(' pr ', ' pour ')
        l = l.replace(' pb ', ' problème ')
        l = l.replace(' putin ', ' putain ')
        l = l.replace(' [Pp]ti?n ', ' putain ')
        l = re.sub(' ps+t ', ' psst ', l)
        l = l.replace(' qqn ', ' quelqu\'un ')
        l = l.replace(' quon ', ' que on ')
        l = l.replace(' qq ', ' quelque ')
        l = re.sub(' [Qq]n?d ', ' quand ', l)
        l = l.replace(' sa me ', ' ça me ')
        l = l.replace(' sa fait ', ' ça fait ')
        l = l.replace(' sa te ', ' ça te ')
        l = l.replace(' scuse ', ' excuse ')
        l = re.sub(' s\'il-te-pla[îi]t ', ' stp ', l)
        l = re.sub(' s\'il-vous-pla[îi]t ', ' svp ', l)
        l = re.sub(' s\'il', ' si il', l, flags=re.I)
        l = re.sub(' svoit ', ' se voit ', l, flags=re.I)
        l = re.sub('T\'', 'Tu ', l)
        l = re.sub('teletravail', 'télétravail', l)
        l = re.sub('television', 'télévision', l)
        l = re.sub('[^ ][Tt]\'?sais', ' tu sais ', l)
        l = l.replace(' trql ', ' tranquille ')
        l = l.replace(' tlmt ', ' tellement ')
        l = l.replace(' ttes ', ' toutes ')
        l = l.replace(' tte ', ' toute ')
        l = re.sub(' teu?pu ', ' pute ', l)
        l = re.sub(' tjr?s? ', ' toujours ', l)
        l = l.replace(' tps ', ' temps ')
        l = re.sub(' [Tt] un ', ' tu es ', l)
        l = l.replace(' ts ', ' tous ')
        l = l.replace(' ns ', ' nous ')
        l = l.replace(' ms ', ' mais ')
        l = re.sub(' v\'?nir ', ' venir ', l)
        l = l.replace('vousêtes', 'vous êtes')
        l = l.replace(' Vs ', ' Vous ')
        l = l.replace('Vousle ', ' Vous le ')
        l = re.sub(' ([Jj]e|il|elle|nous|[Vv]ous|[Ii]ls) vs ', ' \g<1> vous ', l)
        l = re.sub(' vs ([^ ]ez) ', ' vous \g<1> ', l)
        l = re.sub(' (?:veneres?|vnr)  ', ' énervé ', l)
        l = re.sub(' [Vv]oila ', ' voilà ', l)
        l = l.replace(' trp ', ' trop ')
        l = l.replace('at° ', 'ation ')
        l = l.replace('i° ', 'ition ')
        l = re.sub('mnt ', 'ment ', l)
        l = l.replace(' toussa ', ' tout ça ')
        l = re.sub(' we?s+h+ ', ' wesh ', l)
        l = re.sub(' we?s+h+ ', ' bonjour ', l)
        l = re.sub(' Y[\' ]?a plus ', ' Il n\'y a ', l)
        l = re.sub(' y[\']?a plus ', ' il n\'y a ', l)
        l = re.sub(' Y[\' ]?a ', ' Il y a ', l)
        l = re.sub(' y[\']?a ', ' il y a ', l)
        l = re.sub(' y[\']?avait ', ' il y avait ', l)
        l = re.sub('\+en\+', ' plus en plus ', l)

        # bug MeLT
        # l = re.sub(' [0-9]+(e|ème|ième|eme|ieme) ',' énième ',l)
        # l = re.sub('( F .+) G ','\g<1> F ',l)
        # l = re.sub('[\{\}]',' . ',l)

        # MWE
        l = re.sub('  +', ' ', l)
        for t in (t for t in self.t_mwe if '_' in t):
            t2replace = t.replace('_', ' ')
            l = re.sub(t2replace, t, l, flags=re.I)
            t2replace = t.replace('_', '-')
            l = re.sub(t2replace, t, l, flags=re.I)

        # locutions
        l = re.sub(' je suis morte? ', ' lol ', l)
        l = re.sub(' il m\'a tuée? ', ' lol ', l)
        l = l.replace(' il me tue ', ' lol ')
        l = re.sub('(c\'est pas )?[Pp]as comme s(i|\'il) ', 'hypocrisie ', l)
        l = re.sub('dit celui qui ', ' dit l\'hypocrite ', l)
        l = re.sub('pas (?:des masses|beaucoup|très|vraiment)', 'peu', l)
        l = l.replace('hors du commun', ' exceptionnel ')
        l = re.sub(' de dingues? ', ' exceptionnel ', l)
        l = l.replace(' mauvais oeil ', ' mauvais_oeil ')
        l = l.replace(' faux air ', ' faux_air ')
        l = re.sub(' (aucun |sans |nul )+doutes? (?:aucun)', ' surement ', l)

        # accents (simples)
        l = l.replace(' face a ', ' face à ')
        l = l.replace(' pas a ', ' pas à ')
        l = l.replace(' des que ', ' dès que ')

        # négations
        l = re.sub(' noo+n ', ' non ', l)
        if bool_remove_neg:
            l = l.replace(' Pas ', ' MaNeg ')
            l = re.sub(' (?:ne |n\')(.+?) (jamais|pas|plus|rien) ', ' MaNeg \g<1> ', l)
            l = re.sub(' ne (pas|plus|rien) ', ' MaNeg ', l)
            l = re.sub(' pas non plus ', ' MaNeg ', l)
            l = re.sub(' non (pas|plus|rien) ', ' MaNeg ', l)
            l = re.sub('( tellement| vraiment)? pas ', ' MaNeg ', l)
            l = re.sub(' (aucun|sans) ', ' MaNeg ', l)
            l = l.replace(' aussi peu de ', ' MaNeg ')
            l = l.replace(' ni ', ' MaNeg ')

            l = re.sub(' (absence|manque|disparition|perte) (de|d\'|des|du)', ' MaNomNeg ', l, flags=re.I)

        # temps
        l = re.sub(' demie? ?-?heures? ', ' 30 minutes ', l, flags=re.I)
        l = re.sub(' trois quarts?-? ?d.heures? ', ' 45 minutes ', l, flags=re.I)
        l = re.sub(' un quart-? ?d.heures? ', ' 15 minutes ', l, flags=re.I)

        # propre au corpus
        # le foot/TV
        # ~ l = re.sub(' mi-tps ',' mi-temps ',l,flags=re.I)
        # ~ l = re.sub(' (première|1ère|1e|1er) période',' première mi-temps ',l,flags=re.I)
        # ~ l = re.sub(' (deuxième|seconde|2ème|2[éèe]+|2nde) période',' deuxième mi-temps ',l,flags=re.I)
        # ~ l = re.sub(' en direct ',' en_direct ',l,flags=re.I)
        # ~ l = re.sub(' (je|tu|il|ont?) passe ',' \g<1> semble ',l,flags=re.I)
        # ~ l = re.sub(' (le|en|un) contre ',' \g<1> contre_foot ',l,flags=re.I)
        # ~ l = re.sub(' coupe du monde ',' coupe_du_monde ',l,flags=re.I)
        # ~ l = re.sub(' cdm ',' coupe_du_monde ',l,flags=re.I)
        # ~ l = re.sub(' coupe d.europe ',' coupe_d_europe ',l,flags=re.I)
        # ~ l = re.sub(' coupe de france ',' coupe_de_france ',l,flags=re.I)
        # ~ l = re.sub(' passe (D|dé) ',' passe_décisive ',l,flags=re.I)
        # ~
        # ~ l = re.sub(' stade de france ',' stade_de_france ',l,flags=re.I)
        # ~
        # ~ l = re.sub(' #?bleus ',' équipe_de_france ',l,flags=re.I)
        # ~ l = re.sub(' fran[çc]ais ',' équipe_de_france ',l,flags=re.I)
        # ~ l = re.sub(' @equipedefrance ',' équipe_de_france ',l,flags=re.I)
        # ~ l = re.sub(' #teamfrance ',' équipe_de_france ',l,flags=re.I)
        # ~ l = re.sub(' #?EDF ',' équipe_de_france ',l,flags=re.I)
        # ~ l = re.sub('[ée]quipe de france ',' équipe_de_france ',l,flags=re.I)
        # ~ l = re.sub(' france ',' équipe_de_france ',l,flags=re.I)
        # ~ l = re.sub(' 🇫🇷 ',' équipe_de_france ',l,flags=re.I)
        # ~
        # ~ l = re.sub(' #?allemands ',' équipe_d_allemagne ',l,flags=re.I)
        # ~ l = re.sub(' #?mannschaft ',' équipe_d_allemagne ',l,flags=re.I)
        # ~ l = re.sub(' #?diemannschaft ',' équipe_d_allemagne ',l,flags=re.I)
        # ~ l = re.sub('(^|\'| )Allemagne ','\g<1>équipe_d_allemagne ',l,flags=re.I)
        # ~ l = re.sub(' 🇩🇪',' équipe_d_allemagne ',l,flags=re.I)
        # ~ l = re.sub(' coup d.envoi ',' coup_d_envoi ',l,flags=re.I)
        # ~ l = re.sub(' #?g+o+a+l+ ',' but ',l,flags=re.I)
        # ~ l = re.sub(' #?b+u+t+ ',' but ',l,flags=re.I)
        # ~
        # ~ l = re.sub(' bravo ',' MonEncouragement ',l,flags=re.I)
        # ~ l = re.sub(' alle[rz] (?: *les)? *équipe[^ ]+',' MonEncouragement ',l,flags=re.I)
        # ~ l = re.sub(' #alle[rz]les[^ ]+',' MonEncouragement ',l,flags=re.I)
        # ~ l = re.sub(' forza (?: *les)? *equipe[^ ]+',' MonEncouragement ',l,flags=re.I)
        # ~
        # ~ # score
        # ~ l = re.sub(' ([0-90️⃣] ?- ?[0-90️⃣]) ',' score_\g<1> ',l,flags=re.I)

        # ~
        # ~ l = re.sub('(ouvre_le_score|ouvrir_le_score|ouvre_la_marque|ouvrir_la_marque|marquer|marque)','\g<1> but ',l)
        # ~

        adv = '(toujours |très |trop |aussi |vraiment )'

        if bool_normalize_polarity:
            # insulte/jugement neg
            l = re.sub(' ptn ', ' ', l)  # c'est une interjection
            l = re.sub(
                ' (b[aâ]tard|blaireaux?|brêle|bouffon|connard|connasse|connerie|fils de pute|con|conne|keko|looser|s?fdp|merde|ntm|ordure|pute|salaud|salopard)s? ',
                ' MonInsulte ', l)
            l = re.sub(' est ' + adv + '*(cheum|mauvais|môche|nul) ', ' MonInsulte ', l)
            # l = re.sub(' rate ',' MonInsulte ',l)
            # l = re.sub(' (j(e n)?\'aime|je (ne )?l\'aime|j(e n)?\'adore) pas ',' MonInsulte ',l)
            l = re.sub(' (a )?fait (nous )?(rien|gainer) ', ' MonInsulte ', l)
            l = re.sub(' on aura tout vu ', ' MonInsulte ', l)
            l = re.sub(' pire joueur ', ' MonInsulte ', l)
            l = re.sub(' (mauvaise?|piètre) (travail|prestation|retour) ', ' MonInsulte ', l)

            # compliment/jugement pos
            l = re.sub('[💯👌💪🚀]', ' MonCompliment ', l)
            l = re.sub('([:\.]) +(Eno+rme|incroyable) ', ' MonCompliment ', l)
            l = re.sub(' #(respect) ', ' MonCompliment ', l)
            # l = re.sub(' grâce à ',' MonCompliment ',l)
            l = re.sub(' (je aime|je l\'aime|je adore) ', ' MonCompliment ', l)
            l = re.sub(' (beau|belle|bon|bonne|énorme|gros|grosse) (boulot|travail|match|prestation|retour) ',
                       ' MonCompliment ', l)
            l = re.sub(' est ' + adv + '*(bon|chaud|fort|partout|au dessus|au rdv|au rendez-vous|solide|vif) ',
                       ' MonCompliment ', l)
            l = re.sub(' a (du|un) talent ', ' MonCompliment ', l)
            l = re.sub(' au (dessus du lot|top) ', ' MonCompliment ', l)
            l = re.sub(' (mis|met) (de|du|le) (sale|feu) ', ' MonCompliment ', l)
            l = re.sub(' (a )?fait (moi |nous )?(la diff(?:érence)?|du sale?|le taff?|rêver|plaisir) ',
                       ' MonCompliment ', l)
            l = re.sub(
                ' c\'est ' + adv + '(un|une|le|la) (bête|boss|excellent joueur|machine|monstre|classe|meilleur) ',
                ' MonCompliment ', l)

        # les villes,pays...

        l = re.sub('[^ \.]+\.\.\.+( URL)?( Pers)? *$', ' …', l)
        l = re.sub('\.\.\.+', ' …', l)
        # ~ l = re.sub(' [a-z]+ …',' …',l)
        # l = re.sub(' etc.',' …',l)
        # l = re.sub('…',' … ',l)
        l = re.sub('^<([^>]+)>', '\g<1>', l)
        l = re.sub(' [—–\|]+ ', ' . ', l)

        # on vire tous les autres caractères
        l = re.sub('([^A-ZÇÂÀÄÉÊÈËÎÌÔÙÛa-zçâàäáéèêëîïìíôòóöûüùú0-9 #_\-\'",;:\(\)\n\.!\?…@]+)', ' ', l)

        # on met une espace avant-après les ponctuations
        l = re.sub('([",;:\(\)\n\.!\?…])', ' \g<1> ', l)

        # on met un \n en fin de phrase
        l = re.sub('([\.!\?…])', ' \g<1>\n', l)

        # ~ if re.search('â|é|è|ê|ë|î|ï|ô|ù|û',l) is None: # aucun accent
        # ~ l = ' '.join(self.Reaccentue(w) for w in  l.split(' ') )

        l = re.sub('  +', ' ', l)

        return l

    def Tagging(self, text):

        # multi-words expressions ##########################################
        text = re.sub('  +', ' ', text)
        for t in (t for t in self.t_mwe if '_' in t):
            t2replace = t.replace('_', ' ')
            text = re.sub(t2replace, t, text, flags=re.I)
            t2replace = t.replace('_', '-')
            text = re.sub(t2replace, t, text, flags=re.I)

        # il faut enlever les mentions tweeter
        text = re.sub('@', 'Quidam_', text)
        # il faut enlever les #
        text = re.sub('#', ' ', text)

        t_tagging = []
        for l_w in self.tagger.tag_text(text):
            t_l = l_w.split('\t')
            if not len(t_l) == 3:
                print('pb tagging, incorrect format:', l_w)
            else:
                w, t, l = t_l
            l = re.sub('[^\|]+\|', '', l)
            t = re.sub(':.+', '', t)

            t_tagging.append((w, t, l))
        return t_tagging

    def __init__(self, lang='fr', bool_tweet=False, f_mwe=None, actants=None, debug=0, dict_lexicon=None, file_BD=None,
                 tagdir=TAGDIR):
        Info('This is OpinEx ' + VERSION + ' / Licensed for test to ' + CLIENTNAME)
        Info('vincent.claveau@irisa.fr')
        Info('language is set to ' + lang)

        global lib_path
        lib_path = os.path.dirname(__file__)

        global DEBUG
        DEBUG = debug

        Warn('lib_path is set to ' + str(lib_path))
        Warn('DEBUG is set to ' + str(DEBUG))

        # ~ if not get_mac()==int(MAC_ADDRESS):
        # ~ Warn('FAILED: License not accepted, please contact vincent.claveau@irisa.fr')
        # ~ sys.exit()

        localtime = time.localtime(time.time())
        if False and localtime[
            0] > 2021:  # or (localtime[0] == 2018 and localtime[1]>11) or localtime[0]<2015 or (localtime[0] == 2015 and localtime[1]<10):
            Warn('FAILED: License not accepted, please contact vincent.claveau@irisa.fr')
            # return 12
            sys.exit()

        # ~ if get_mac() not in T_MAC_ADDRESS:
        # ~ Warn('FAILED: License not accepted, please contact vincent.claveau@irisa.fr')
        # ~ sys.exit()#return 12

        # ~ if str(get_uuid()) not in T_UUID_ADDRESS:
        # ~ Warn('FAILED: License not accepted, please contact vincent.claveau@irisa.fr')
        # ~ #return 12
        # ~ sys.exit()

        self.debug, self.lang, self.bool_tweet = debug, lang, bool_tweet
        tagopt = ' -token -lemma -no-unknown -sgml '

        self.h_short2long = {}

        if actants is not None: self.h_actants = Read_actants(actants)

        if lang == 'fr':
            self.t_mwe = ['d\'abord', 'd\'accord', 'd\'ailleurs', 'd\'après', 'd\'autant', 'd\'oeuvre', 'c\'est-à-dire',
                          'moi-même', 'toi-même', 'lui-même', 'elle-même', 'nous-mêmes', 'vous-mêmes', 'eux-mêmes',
                          'elles-mêmes', 'par-ci', 'par-là', 'D\'abord', 'D\'accord', 'D\'ailleurs', 'D\'après',
                          'D\'autant', 'D\'uvre', 'D\'oeuvre', 'C\'est-à-dire', 'Moi-même', 'Toi-même', 'Lui-même',
                          'Elle-même', 'Nous-mêmes', 'Vous-mêmes', 'Eux-mêmes', 'Elles-mêmes', 'Par-ci', 'Par-là']
        if f_mwe is not None and os.path.exists(f_mwe):
            f_loc = codecs.open('tmp.opinex.loc', 'w', 'utf-8')
            for line in codecs.open(f_mwe, 'r', 'utf-8', errors='ignore'):
                wf, tag, l = line.rstrip('\r\n').split('\t')
                self.t_mwe.append(wf)
                f_loc.write(line)
            for actant in self.h_actants:
                pp = actant.strip(' ')
                f_loc.write(pp + '\tNAM\t' + pp + '\n')
            f_loc.close()
            tagopt += '-lex tmp.opinex.loc '
        else:
            Warn('Could not find MWE file ' + f_mwe)

        if dict_lexicon is None: dict_lexicon = lib_path + '/opinex.data'
        self.h_polarity_lexicon = Read_lexicons(self.lang, dict_lexicon)

        ########################################################################
        import treetaggerwrapper

        self.tagger = treetaggerwrapper.TreeTagger(TAGLANG=self.lang, TAGDIR=tagdir, TAGOPT=tagopt)

        self.h_BD = None

        if self.lang == 'fr':
            self.set_noun = set(['NOM'])
            self.set_verb = set(['VER'])
            self.set_adj = set(['ADJ'])
            self.set_adv = set(['ADV'])

            self.set_magn = set('amplement énormément franchement très vachement vraiment tellement'.split(' '))

            if file_BD is not None:
                Info('DB file chosen: ' + file_BD)
                self.h_BD = ReadPickle(file_BD)

    def Opinion_baseline(self, text, t_tagged=None, bool_tweet=None):

        positive, negative = 0, 0
        if bool_tweet == None: bool_tweet = self.bool_tweet

        if t_tagged is None:
            if bool_tweet: text = self.PreProcessTweet(text, bool_remove_citation=True)
            t_tagged = self.Tagging(text)

        h_found_pos, h_found_neg, h_magn = defaultdict(int), defaultdict(int), defaultdict(int)

        for m in self.h_polarity_lexicon['re_pos'].finditer(text):
            # print m.group(1)
            h_found_pos[m.group(1)] += 1
            positive += 1
        for m in self.h_polarity_lexicon['re_neg'].finditer(text):
            # print m.group(1)
            h_found_neg[m.group(1)] += 1
            negative += 1

        for i, t_elem in enumerate(t_tagged):
            w, t, l = t_elem[0:3]  # car des fois je mets une 4e colonne (stem)

            # print(w,t,l)

            points_pos, points_neg = 0, 0
            if l in ['bravo', 'MonAmour', 'monamour', 'MonCompliment', 'moncompliment', 'MonEncouragement',
                     'monencouragement']:  # ,'lol']:
                points_pos = 2
                # h_found_pos[w]+=2

            elif l in ['MonInsulte', 'moninsulte', 'paslol']:
                points_neg = 2

            # Lidilem
            elif t in self.set_noun and l in self.h_polarity_lexicon['N']:
                if self.h_polarity_lexicon['N'][l][2] == 'positif':
                    points_pos = 1
                elif self.h_polarity_lexicon['N'][l][2] == 'négatif':
                    points_neg = 1

            elif t in self.set_adj and l in self.h_polarity_lexicon['ADJ']:
                if self.h_polarity_lexicon['ADJ'][l][2] == 'positif':
                    points_pos = 1
                elif self.h_polarity_lexicon['ADJ'][l][2] == 'négatif':
                    points_neg = 1

            elif t in self.set_verb and l in self.h_polarity_lexicon['V']:
                if self.h_polarity_lexicon['V'][l][2] == 'positif':
                    points_pos = 1
                elif self.h_polarity_lexicon['V'][l][2] == 'négatif':
                    points_neg = 1

            elif t in self.set_adv and l in self.h_polarity_lexicon['ADV']:
                if self.h_polarity_lexicon['ADV'][l][2] == 'positif':
                    points_pos = 1
                elif self.h_polarity_lexicon['ADV'][l][2] == 'négatif':
                    points_neg = 1

            # ANEW
            elif (t in self.set_adj or t in self.set_noun or t in self.set_verb) and l in self.h_polarity_lexicon[
                'ANEW'] and self.h_polarity_lexicon['ANEW'][l][0] >= 7:
                points_pos = 1
            elif (t in self.set_adj or t in self.set_noun or t in self.set_verb) and l in self.h_polarity_lexicon[
                'ANEW'] and self.h_polarity_lexicon['ANEW'][l][0] <= 3:
                points_neg = 1

            # FEEL
            # ~ elif (t in self.set_adj or t in self.set_noun or t in self.set_verb) and l in self.h_polarity_lexicon['FEEL'] and self.h_polarity_lexicon['FEEL'][l][0] == 'positive':
            # ~ points_pos=1
            # ~ elif (t in self.set_adj or t in self.set_noun or t in self.set_verb) and l in self.h_polarity_lexicon['FEEL'] and self.h_polarity_lexicon['FEEL'][l][0] == 'negative':
            # ~ points_neg=1

            if i > 0 and t_tagged[i - 1][0] == 'MaNeg' or i < len(t_tagged) - 1 and t_tagged[i + 1][0] == 'MaNeg':
                if points_pos > 0:
                    h_found_neg['MaNeg' + '_' + l] += points_pos
                    negative += points_pos
                if points_neg > 0:
                    h_found_pos['MaNeg' + '_' + l] += points_neg
                    positive += points_neg
            else:
                if points_neg > 0:
                    h_found_neg[l] += points_neg
                    negative += points_neg
                if points_pos > 0:
                    h_found_pos[l] += points_pos
                    positive += points_pos

            if (points_neg > 0 or points_pos > 0) and ((i > 0 and t_tagged[i - 1][2] in self.set_magn) or (
                    i < len(t_tagged) - 1 and t_tagged[i + 1][2] in self.set_magn)):
                h_magn[t_tagged[i - 1][2] + '_' + l] += 1

        if positive > negative:
            opinion = 'positive'
        elif positive < negative:
            opinion = 'negative'
        elif positive == 0:
            opinion = 'objective'
        else:
            opinion = 'mixed'

        return opinion, h_found_pos, h_found_neg, h_magn
