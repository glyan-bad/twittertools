import json
import functools
import pandas as pd
import gzip, glob, sys, os

# This file contains code that helps to convert tweets that were collected by Vincent Claveau.
# The input format is compressed gzip files containing json responses from twitter API
# The output is a more user friendly parquet format.

def Info(output='', ending='\n'):  # print(output, file=sys.stderr)
    sys.stderr.write(str(output) + ending)
    sys.stderr.flush()



def convertLegacy(dir, output_path: str):
    output_path += '/' if not output_path.endswith('/') else ""

    if not os.path.exists(output_path):
        print(output_path, 'does not exists. Creating it')
        os.mkdir(output_path)

    for fn_gz in sorted(glob.glob(dir + '/*.jsonl.gz')):
        Info('Processing file ' + fn_gz, ending='\n')
        try:
            with gzip.open(fn_gz, 'rt', encoding='utf-8', errors='ignore') as fn_jsonl:
                file_path = fn_jsonl.name.split('/')
                file_name = file_path[len(file_path) - 1]
                output_name = file_name.split('.')[0] + '.parquet'
                jsonl_content = fn_jsonl.read()
                data = [json.loads(jline) for jline in jsonl_content.splitlines()]
                result = convert(data)
                output_df = result[0].join(result[1].set_index('id'), on='id')
                output_df.to_parquet(output_path + output_name)
        except:
            Info("FAILURE")


def getFullTxt(tweet):
    res = []
    ### text ###
    if 'extended_tweet' in tweet:
        res = [tweet['id'], 'ORIG', tweet['created_at'], tweet['user']['id'], tweet['text'],
               tweet['extended_tweet']['full_text'], -1]
    else:
        res = [tweet['id'], 'ORIG', tweet['created_at'], tweet['user']['id'], tweet['text'], tweet['text'], -1]
    if 'retweeted_status' in tweet and tweet['retweeted_status']['lang'] == 'fr':
        if 'extended_tweet' in tweet['retweeted_status']:
            res = [tweet['id'], 'RT', tweet['created_at'], tweet['user']['id'], tweet['text'],
                   tweet['retweeted_status']['extended_tweet']['full_text'], tweet['retweeted_status']['id']]
        else:
            res = [tweet['id'], 'RT', tweet['created_at'], tweet['user']['id'], tweet['text'],
                   tweet['retweeted_status']['text'], tweet['retweeted_status']['id']]
    ### quoted tweet ###
    if 'quoted_status' in tweet and tweet['quoted_status']['lang'] == 'fr':
        if 'extended_tweet' in tweet['quoted_status']:
            res = [tweet['id'], 'QT', tweet['created_at'], tweet['user']['id'], tweet['text'],
                   tweet['quoted_status']['extended_tweet']['full_text'], tweet['quoted_status']['id']]
        else:
            res = [tweet['id'], 'QT', tweet['created_at'], tweet['user']['id'], tweet['text'],
                   tweet['quoted_status']['text'], tweet['quoted_status']['id']]
    res += ["https://twitter.com/twitter/statuses/%s" % (res[0])]
    return res


def extractMedias(id, mediaList):
    res = []
    for media in mediaList:
        res += [[media['id'], media['media_url_https'], media['type'], id]]
    return res


def extractMediaByType(tweet):
    if 'extended_tweet' in tweet and 'entities' in tweet['extended_tweet'] and 'media' in tweet['extended_tweet'][
        'entities']:
        return extractMedias(tweet['id'], tweet['extended_tweet']['entities']['media'])

    elif 'retweeted_status' in tweet and 'extended_tweet' in tweet['retweeted_status'] and 'entities' in \
            tweet['retweeted_status']['extended_tweet'] and 'media' in tweet['retweeted_status']['extended_tweet'][
        'entities']:
        return extractMedias(tweet['id'], tweet['retweeted_status']['extended_tweet']['entities']['media'])

    elif 'quoted_status' in tweet and 'extended_tweet' in tweet['quoted_status'] and 'entities' in \
            tweet['quoted_status']['extended_tweet'] and 'media' in tweet['quoted_status']['extended_tweet'][
        'entities']:
        return extractMedias(tweet['id'], tweet['quoted_status']['extended_tweet']['entities']['media'])
    else:
        return []


def convert(json):
    txts = list(map(lambda x: getFullTxt(x), json))
    tweets = pd.DataFrame.from_records(txts,
                                       columns=['id', 'tweet_type', 'created_at', 'author_id', 'text', 'full_text',
                                                'rt_qt_id', 'tweet_url'])
    media_list = functools.reduce(lambda a, b: a + b, list(map(lambda x: extractMediaByType(x), json)))
    media = pd.DataFrame.from_records(media_list, columns=['media_key', 'media_url', 'type', 'id'])
    return [tweets, media]


if __name__ == "__main__":

    for i in range(1, 64):
        if i < 10:
            convertLegacy('/Users/glyan/workspace/FakeUkraine/Fake_Ukraine_00%i/' % i,
                          '/Users/glyan/workspace/legacy_full/')
        elif i < 100:
            convertLegacy('/Users/glyan/workspace/FakeUkraine/Fake_Ukraine_0%i/' % i,
                          '/Users/glyan/workspace/legacy_full/')
        else:
            convertLegacy('/Users/glyan/workspace/FakeUkraine/Fake_Ukraine_%i/' % i,
                          '/Users/glyan/workspace/legacy_full/')
