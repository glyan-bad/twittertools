import pandas as pd

from twitterTools import *
from utils import *

# Gathers tweets from twitter given a specific twitter query
def gather_tweets(query):
    tweets = explore(Utils.client.search_all_tweets, query=query,
                     tweet_fields=['context_annotations', 'created_at'],
                     user_fields=Utils.user_fields,
                     media_fields=Utils.media_fields,
                     expansions=['attachments.media_keys', 'author_id'],
                     max_results=100)
    tweets_dfs = []
    tweets_medias = []
    tweets_mks = []
    cpt = 0
    while cpt < len(tweets):
        print(cpt)
        current_tweets =list(filter(lambda x: x['attachments'] is not None, tweets[cpt:cpt + 4][0]))
        tweets_dfs += [tweetsListToDF(current_tweets)]
        tweets_mks += [pd.DataFrame.from_records(
            list(map(lambda x: [x['id'], x['attachments']['media_keys']], current_tweets)),
            columns=['id', 'media_key']).explode(column=['media_key'])]
        tweets_medias += [mediaListToDF(tweets[cpt:cpt + 4][1]['media'])]
        cpt += 4

    tweets_df = reduce(pd.concat, tweets_dfs)
    tweets_mk_df = reduce(pd.concat, tweets_mks).join(tweets_df.set_index('id'), on='id')
    tweets_media_df = reduce(pd.concat, tweets_medias)
    result = tweets_mk_df.join(tweets_media_df.set_index('media_key'), on='media_key')
    result = result[['id','text','author_id','created_at','tweet_url','tweet_type','media_key','type','media_url']]
    return result

if __name__ == '__main__':
    df = gather_tweets(query='climat lang:fr -is:retweet has:media')
    print(tabulate(df, headers='keys', tablefmt='psql'))
